
/*********************************************************************
DESARROLLO RAPIDO 2012

********************************************************************/
create database desarrollo2012;
use desarrollo2012;


create table Usuario
(
	idUsuario int not null auto_increment primary key,
	usuario varchar(100) unique not null,
	password varchar(500) not null,
	idTipoUsuario int not null references tipoUsuario(idTipoUsuario)
)Engine=InnoDB;

create table tipoUsuario
(
	idTipoUsuario int not null primary key auto_increment,
	tipo varchar(25) not null
)Engine=InnoDB;

create table Documento
(
	idDocumento int not null auto_increment primary key,
	documento varchar(30),
	idUsuario int not null references Usuario(idUsuario),
	numero int not null unique
)Engine=InnoDB;


create table Pais
(
	idPais int not null auto_increment primary key,
	pais varchar(100) not null,
	ZIP varchar(10) unique
)Engine=InnoDB;


create table UbicacionFisica(
	idUbicacion int not null auto_increment primary key,
	idPais int not null references Pais(idPais),
	idEstado int not null
)Engine=InnoDb;


create table Departamento
(
	idDepartamento int not null auto_increment primary key,
	departamento varchar(100) not null,
	idPais int not null references Pais(idPais)
)Engine=InnoDB;

create table Municipio
(
	idMunicipio int not null auto_increment primary key,
	municipio varchar(100) not null,
	idDepartamento int not null references Departamento(idDepartamento)
)Engine=InnoDB;

/*PARTE DE DIRECCION*/
create table Direccion
(
	idDireccion int not null auto_increment primary key,
	idUsuario int not null references Usuario(idUsuario),
	direccion varchar(200) not null,
	idPais int not null references Pais(idPais),
	fechaInicio datetime not null,
	fechaFinal datetime
)Engine=InnoDB;



/*PARTE DE TELEFONOS*/
create table Telefonos
(
	idTelefono int not null auto_increment primary key,
	idUsuario int not null references Usuario(idUsuario),
	telefono varchar(10)  not null,
	fechaInicio datetime not null,
	fechaFinal datetime not null,
	tipoLinea int not null, /*-----------Enumerada 1.Celular,2.Linea Fija--------------*/
	prefijoPais int not null,
	prefijoProvincialI int,
	prefijoProvinciaN int,
	tipo int not null /*---------1.Telefono usuario, 2. Telefono contacto----------------*/
)Engine=InnoDB;

create table Correo
(
		idCorreo int not null primary key auto_increment,
		idUsuario int not null references Usuario(idUsuario),
		correo varchar(200) not null,
		idLocalizacion int not null,
		fechaInicio datetime,
		fechaFinal datetime
)Engine=InnoDB;

create table CentroEducativo
(
	idCentro int not null auto_increment primary key,
	nombreCentro varchar(200)
)Engine=InnoDB;


create table Carrera
(
	idCarrera int not null auto_increment primary key,
	carrera varchar(140)
)Engine=InnoDB;


create table Especialidad
(
	idEspecialidad int not null auto_increment primary key,
	especialidad varchar(100),
	idCarrera int not null references Carrera(idCarrera)
)Engine=InnoDB;

create table HistorialAcademico
(
	idHistorial int not null primary key auto_increment,
	idUsuario int not null references Usuario(idUsuario),
	fechaInicio datetime not null,
	fechaFinal datetime,
	idCentroEducativo int references CentroEducativo(idCentro),
	idNivelAcademico int not null,
	idCarrera int references Carrera(idCarrera),
	idEspecialidad int references Especialidad(idEspecialidad),
	numAnios int, /*Numero de a�os de titulaci�n*/
	numAniosC int /*Numero de a�os completados*/
)Engine=InnoDB;

/*Tabla que llevar� el registro de las ponderaciones: B�sico, Intermedio, Avanzado*/
create table Ponderacion
(
	idPonderacion int not null auto_increment primary key,
	nivel varchar(30)
)Engine=InnoDB;

create table Idiomas
(
	idIdioma int not null primary key auto_increment,
	Idioma int not null /*ComboBox*/,
	fechaInicio datetime not null,
	fechaFinal datetime,
	idConversacion int references Ponderacion(idPonderacion),
	idComprension int references Ponderacion(idPonderacion),
	idEscrito int references Ponderacion(idPonderacion)
)Engine=InnoDB;

create table IdiomasUsuario
(
	idUsuario int not null references Usuario(idUsuario),
	idIdioma int not null references Idioma(idIdioma)
)Engine=InnoDB;

create table ExperienciaProfesional
(
	idExperiencia int  not null primary key auto_increment,
	idUsuario int not null references Usuario(idUsuario),
	empresa varchar(100),
	fechaInicio datetime,
	fechaFinal datetime,
	idSector int not null,
	idArea int not null,
	idPais int not null references Pais(idPais),
	numEmpleados int,
	puestoInicial varchar(100) not null,
	puestoFinal varchar(100),
	salario varchar(30),
	actividadesDesarrolladas varchar(500),
	jefeInmediato varchar(100),
	puestoJefe varchar(100),
	direccion varchar(500)
)Engine=InnoDB;



create table InfoComplemetaria
(
	idInfoComplementaria int not null primary key auto_increment,
	idUsuario int not null references Usuario(idUsuario),
	disponibilidadViaje boolean not null,
	movilidadNacional boolean not null,
	movilidadInternacional boolean not null,
	salarioMinimo varchar(30),
	idArea int,
	hobbies varchar(300),
	otros varchar(300)
)Engine=InnoDB;

create table HistorialComplementario
(
	idHistorialC int not null primary key auto_increment,
	idUsuario int not null references Usuario(idUsuario),
	nombreCurso varchar(100) not null,
	numHoras int,
	nombreCentro varchar(100) not null,
	idPais int not null references Pais(idPais),
	comentairo varchar(300),
	becasPremios varchar(300),
	fechaInicio datetime not null,
	fechaFinal datetime
)Engine=InnoDB;

create table DatosContacto
(
	idDatosContacto int not null primary key auto_increment,
	idUsuario int not null references Usuario(idUsuario),
	contacto varchar(100) not null,
	contactoPrincipal boolean not null, /* con checkbox*/
	referenciable boolean not null,
	dependencia varchar(300),
	direccion varchar(300),
	idPais int references Pais(idPais),
	idDepto int references Depto(idDepto),
	idMunicipio int references Municipio(idMunicipio),
	resultadoReferencia varchar(500),
	fechaInicio datetime,
	fechaFinal datetime
)Engine=InnoDB;

create table DatosCandidato
(
	idDatosCandidato int not null primary key auto_increment,
	fechaInicio datetime not null,
	idEstado int not null,
	idTipo int not null,
	idMotivo int not null, /*----Contratacion temporal, permanente, medio tiempo*/
	idOrigen int not null,
	nombreRH varchar(100),
	idFuente int,
	nombreContacto varchar(100)
)Engine=InnoDB;

create table curriculum
(
	idCurriculum int not null primary key auto_increment,
	nombre1 varchar(50) not null,
	nombre2 varchar(50),
	apellido1 varchar(50) not null,
	apellido2 varchar(50),
	apellidoCasada varchar(50),
	fechaNacimiento datetime not null,
	idPais int not null references Pais(idPais),
	idTratamiento int not null,
	idSexo int not null,
	idReligion int,
	documentoExtranjero varchar(100),
	ISSS int,
	NIT varchar(20) not null,
	estatura float,
	idUnidadEstatura int not null,
	peso float,
	idUnidadPeso int not null,
	idPaisActual int not null references Pais(idPais)
);


