<?php
@session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
	<title><?php echo $titulo; ?></title>
	<link href="<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<div id="contenido">	
		<div id="header">
			<div id="logo">
				<h1>Desarrollo R&aacute;pido<sup> UDB 2012</sup></h1>
				<p>Descripci&oacute;n del Sitio</p>
			</div>	
			<!-- desactivar barra de busqueda
			<div id="search">				
				<div id="searchform" >
					<form method="post" action="" >
						<p><input class="searchfield" name="search_query" id="keywords" value="B&uacute;squeda..." type="text" />
						<input class="searchbutton" name="submit" value=" Buscar " type="submit" /></p>
					</form>
				</div>
			</div>-->
		</div>
		
		<!-- aqui iba el menu -->
		<?php

		if (isset($_SESSION['usuario']))
 			require_once '../phpclasses/menu.php';
		else
 			require_once 'phpclasses/menu.php';

		ImprimirMenu();
		?>
		<div class="unico">
