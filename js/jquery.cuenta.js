jQuery.fn.cuenta = function(limite)
{
	// Si hay más de un elemento seleccionado 
	// Se recorrén todos ellos
	this.each(function(){
		// Almacenamos el elemento
		elem = $(this);
		// Creando capa con la cuenta
		var cont = $('<div class=\"cont\">'+elem.attr('value').length+'</div>');
		// Insertando capa despues del textarea
		elem.after(cont);
		// Alamacenando la data para recuperarla despues
		elem.data("contador",cont)
		// Metodo de cuenta para cada pulsación del teclado
		elem.keyup(function(){
			// vuelvo a calcular el objeto this al haberme movido a otro ambito
			var elem = $(this);
			// Recupero la data almacenada anteriormente
			var campocontador = elem.data("contador");
			// Actualizo el valor del contador
			campocontador.text(elem.attr('value').length);
			if(limite && (elem.attr('value').length >= limite))
			{
				elem.keypress(function(e){
					if(e.keyCode != 8 && ( elem.attr('value').length >= limite ) )
					{
						e.preventDefault();
					}
				});
			}
		});
	});
	// Siempre se debe retornar this
	return this;
}
